package com.target.targetcasestudy.ui.common

import androidx.recyclerview.widget.DiffUtil

abstract class BaseDiff<M>(private val oldList: List<M>, private val newList: List<M>) : DiffUtil.Callback() {
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition] == newList[newItemPosition]

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size
}