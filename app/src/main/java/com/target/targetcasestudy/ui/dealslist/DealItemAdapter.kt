package com.target.targetcasestudy.ui.dealslist

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.ui.common.BaseDiff

class DealsListAdapter(private val listener: OnItemClicked) : RecyclerView.Adapter<DealsListAdapter.ViewHolder>()  {

    private val list: MutableList<DealItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsListAdapter.ViewHolder {
        val view = DealListItem(parent.context)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: DealsListAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

    fun updateList(dealsList: List<DealItem>) {
        val diff = DealsDiff(this.list, dealsList)
        val diffResults = DiffUtil.calculateDiff(diff)
        this.list.clear()
        this.list.addAll(dealsList)
        diffResults.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(private var dealItem: DealListItem) :
        RecyclerView.ViewHolder(dealItem) {
        init {
            dealItem.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        fun bind(data: DealItem) {
            dealItem.updateUi(data)
            dealItem.setOnClickListener {
                listener.onItemClicked(data)
            }
        }
    }
}

class DealsDiff(
    private val oldList: List<DealItem>,
    private val newList: List<DealItem>
) : BaseDiff<DealItem>(oldList, newList) {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id &&
                oldList[oldItemPosition].title == newList[newItemPosition].title
    }
}

interface OnItemClicked{
    fun onItemClicked(data: DealItem)
}