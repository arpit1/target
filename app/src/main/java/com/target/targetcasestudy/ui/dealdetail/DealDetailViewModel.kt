package com.target.targetcasestudy.ui.dealdetail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.target.targetcasestudy.api.ioScope
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.domain.DealListDataUseCase
import kotlinx.coroutines.launch
import com.target.targetcasestudy.data.model.Result
import com.target.targetcasestudy.domain.GetDealDetailUseCase

class DealDetailViewModel @ViewModelInject constructor(
    private val getDealDetailUseCase: GetDealDetailUseCase
): ViewModel() {

    var dealsList: MutableLiveData<DealItem> = MutableLiveData()
    var message: MutableLiveData<String> = MutableLiveData()

    fun getDealsDetail(id: String) {
        ioScope.launch {
            when (val result = getDealDetailUseCase.fetchDealDataFromRepo(id)) {
                is Result.Success -> {
                    dealsList.postValue(result.data)
                }
                is Result.Error -> {
                    message.postValue(result.exception.message)
                }
            }
        }
    }
}