package com.target.targetcasestudy.ui.dealdetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.target.targetcasestudy.R
import com.target.targetcasestudy.ui.common.setVisible
import com.target.targetcasestudy.ui.common.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_deal_item.*

@AndroidEntryPoint
class DealDetailFragment : Fragment(R.layout.fragment_deal_item) {

    private val viewModel by viewModels<DealDetailViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = arguments?.getString("dealItemId")

        id?.let {
            viewModel.getDealsDetail(it)
            progress_bar.setVisible(true)
            viewModel.dealsList.observe(viewLifecycleOwner) { dealDetail ->
                progress_bar.setVisible(false)
                deal_detail_view.updateUi(dealDetail)
            }

            viewModel.message.observe(viewLifecycleOwner) {
                progress_bar.setVisible(false)
                activity?.showToast(it)
            }
        }
    }
}
