package com.target.targetcasestudy.ui.dealslist

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import com.target.targetcasestudy.ui.common.inflate
import com.target.targetcasestudy.R
import com.target.targetcasestudy.data.model.DealItem
import kotlinx.android.synthetic.main.view_deal_list.view.*

class DealListView @JvmOverloads constructor(
    context: Context,
    attributes: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributes, defStyleAttr), OnItemClicked {

    private val dealsListAdapter: DealsListAdapter = DealsListAdapter(this)

    init {
        inflate(R.layout.view_deal_list)
        dealListRecyclerView.adapter = dealsListAdapter
        dealListRecyclerView.setHasFixedSize(false)
    }

    fun updateUi(dealList: List<DealItem>) {
        dealsListAdapter.updateList(dealList)
        dealsListAdapter.notifyDataSetChanged()
    }

    override fun onItemClicked(data: DealItem) {
        val bundle = bundleOf("dealItemId" to data.id.toString())
        Navigation.findNavController(this).navigate(R.id.dealDetailFragment, bundle)
    }
}