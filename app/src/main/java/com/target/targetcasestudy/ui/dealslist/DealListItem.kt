package com.target.targetcasestudy.ui.dealslist

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import com.target.targetcasestudy.R
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.ui.common.inflate
import kotlinx.android.synthetic.main.row_layout_deal_list_item.view.*

class DealListItem @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttr) {

    init {
        inflate(R.layout.row_layout_deal_list_item)
    }

    fun updateUi(data: DealItem) {
        product_title.text = data.title
        product_price.text = data.regular_price?.display_string
        aisle.text = data.aisle

        Picasso.get().load(data.image_url)
            .placeholder(R.drawable.ic_launcher_foreground)
            .fit()
            .centerCrop()
            .into(product_image)
    }
}