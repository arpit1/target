package com.target.targetcasestudy.ui.dealslist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.target.targetcasestudy.R
import com.target.targetcasestudy.ui.common.setVisible
import com.target.targetcasestudy.ui.common.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_deal_list.*

@AndroidEntryPoint
class DealListFragment : Fragment(R.layout.fragment_deal_list) {

    private val viewModel by viewModels<DealListViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress_bar.setVisible(true)
        viewModel.dealsList.observe(viewLifecycleOwner) {
            deal_list_view.updateUi(it)
            progress_bar.setVisible(false)
        }

        viewModel.message.observe(viewLifecycleOwner) {
            activity?.showToast(it)
        }
    }

}
