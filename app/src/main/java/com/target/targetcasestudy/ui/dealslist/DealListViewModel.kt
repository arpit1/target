package com.target.targetcasestudy.ui.dealslist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.target.targetcasestudy.api.ioScope
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.domain.DealListDataUseCase
import kotlinx.coroutines.launch
import com.target.targetcasestudy.data.model.Result

class DealListViewModel @ViewModelInject constructor(
    private val dealListDataUseCase: DealListDataUseCase
): ViewModel() {

    var dealsList: MutableLiveData<List<DealItem>> = MutableLiveData()
    var message: MutableLiveData<String> = MutableLiveData()

    init {
        getDealsList()
    }

    private fun getDealsList() {
        ioScope.launch {
            when (val result = dealListDataUseCase.fetchDataFromRepo()) {
                is Result.Success -> {
                    dealsList.postValue(result.data.products)
                }
                is Result.Error -> {
                    dealsList.postValue(emptyList())
                    message.postValue(result.exception.message)
                }
            }
        }
    }
}