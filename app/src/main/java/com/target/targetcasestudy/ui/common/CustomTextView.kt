package com.target.targetcasestudy.ui.common

import android.content.Context
import android.graphics.Canvas
import android.text.Layout
import android.util.AttributeSet
import android.util.TypedValue
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.facebook.fbui.textlayoutbuilder.TextLayoutBuilder
import com.facebook.fbui.textlayoutbuilder.TextLayoutBuilder.MEASURE_MODE_EXACTLY
import com.facebook.fbui.textlayoutbuilder.glyphwarmer.GlyphWarmerImpl
import com.target.targetcasestudy.R


class CustomTextView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
): ViewGroup(context, attributeSet, defStyleAttr) {

    private val messageTextLayoutBuilder: TextLayoutBuilder
    private var messageLayout: Layout? = null
    private var message: String? = null
    private var messageLeft: Float = 0f
    private var messageTop: Float = 0f

    private val dimen50dp: Int
    private val dimen24dp: Int

    init {
        setWillNotDraw(false)
        setBackgroundColor(ContextCompat.getColor(context, R.color.white))

        dimen24dp = dpToPx(5f, context)
        dimen50dp = dpToPx(5f, context)

        messageTextLayoutBuilder = TextLayoutBuilder()
            .setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
            .setTextSize(spToPx(16f, context))
            .setShouldWarmText(true)
            .setAlignment(Layout.Alignment.ALIGN_NORMAL)
            .setGlyphWarmer(GlyphWarmerImpl())
    }

    fun setMessage(message: String?) {
        if (this.message.equals(message)) {
            return
        }
        messageLayout = null
        this.message = message

        requestLayout()
        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)

        val messageMaxWidth = width - dimen50dp * 2
        println("xhxhxhx "+height)
        messageLayout = messageTextLayoutBuilder
            .setText(message)
            .setWidth(messageMaxWidth)
            .setMaxLines(200)
            .build()

        setMeasuredDimension(width, height)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val parentLeft = 0
        val parentTop = 0
        val parentRight = right - left
        val parentBottom = bottom - top

        val currentTop = dimen24dp

        messageLayout?.let {
            messageLeft = parentLeft + (parentRight - parentLeft - it.width) / 2f
            messageTop = currentTop.toFloat()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        messageLayout?.let {
            canvas.save()
            canvas.translate(messageLeft, messageTop)
            it.draw(canvas)
            canvas.restore()
        }
    }

    fun dpToPx(dp: Float, context: Context): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp,
            context.resources.displayMetrics
        )
            .toInt()
    }

    fun spToPx(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp,
            context.resources.displayMetrics
        )
            .toInt()
    }

}