package com.target.targetcasestudy.ui.dealdetail

import android.content.Context
import android.graphics.Paint
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import com.target.targetcasestudy.R
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.ui.common.inflate
import kotlinx.android.synthetic.main.view_deal_detail.view.*

class DealDetailView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttr) {

    init {
        inflate(R.layout.view_deal_detail)
    }

    fun updateUi(data: DealItem) {
        Picasso.get().load(data.image_url)
            .placeholder(R.drawable.ic_launcher_foreground)
            .resize(800, 800)
            .centerInside()
            .into(product_image)

        product_title.text = data.title
        product_desc.text = data.description
//        product_desc.setMessage(data.description)

        regular_price.text =
            resources.getString(R.string.regular_price, data.regular_price?.display_string)
        if(data.sale_price != null) {
            regular_price.paintFlags = regular_price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
        discounted_price.text = data.sale_price?.display_string
    }
}