package com.target.targetcasestudy.data.repository

import com.target.targetcasestudy.api.DealApi
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.data.model.Result
import com.target.targetcasestudy.exception.NoInternetException
import com.target.targetcasestudy.exception.TimeoutException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DealDetailRepository @Inject constructor(private val dealApi: DealApi) : BaseRepository() {

    suspend fun fetchDealDetail(id: String): Result<DealItem>? {
        return try {
            val response = safeApiCall(
                call = {
                    dealApi.getDealsDetail(id)
                },
                errorMessage = "Failed to fetch Details"
            )

            response?.let {
                Result.Success(it)
            }
        } catch (exception: TimeoutException) {
            Result.Error(exception)
        } catch (exception: NoInternetException) {
            Result.Error(exception)
        } catch (exception: Exception) {
            Result.Error(exception)
        }
    }
}