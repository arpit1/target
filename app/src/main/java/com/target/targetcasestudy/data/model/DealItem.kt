package com.target.targetcasestudy.data.model

data class DealItem(
    var id: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var image_url: String? = null,
    var regular_price: RegularPrice? = null,
    var sale_price: SalePrice? = null,
    var aisle: String? = null
)

data class RegularPrice(
    var amount_in_cents: String? = null,
    var currency_symbol: String? = null,
    var display_string: String? = null
)

data class SalePrice(
    var amount_in_cents: String? = null,
    var currency_symbol: String? = null,
    var display_string: String? = null
)