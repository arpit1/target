package com.target.targetcasestudy.data.repository

import javax.inject.Inject
import javax.inject.Singleton
import com.target.targetcasestudy.api.DealApi
import com.target.targetcasestudy.api.ServerResponse
import com.target.targetcasestudy.data.model.DealItem
import java.lang.Exception
import com.target.targetcasestudy.data.model.Result
import com.target.targetcasestudy.exception.NoInternetException
import com.target.targetcasestudy.exception.TimeoutException

@Singleton
class DealRepository @Inject constructor(private val dealApi: DealApi) : BaseRepository() {

    suspend fun fetchDealDataFromRepo(): Result<ServerResponse<DealItem>>? {
        return try {
            val response = safeApiCall(
                call = {
                    dealApi.getDealsList()
                },
                errorMessage = "Failed to fetch Details"
            )

            response?.let {
                Result.Success(it)
            }
        } catch (exception: TimeoutException) {
            Result.Error(exception)
        } catch (exception: NoInternetException) {
            Result.Error(exception)
        } catch (exception: Exception) {
            Result.Error(exception)
        }
    }
}