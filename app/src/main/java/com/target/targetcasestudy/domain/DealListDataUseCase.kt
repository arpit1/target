package com.target.targetcasestudy.domain

import com.target.targetcasestudy.api.ServerResponse
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.data.model.Result
import com.target.targetcasestudy.data.repository.DealRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DealListDataUseCase @Inject constructor(
    private val dealsRepository: DealRepository
) {
    suspend fun fetchDataFromRepo(): Result<ServerResponse<DealItem>>? {
        return dealsRepository.fetchDealDataFromRepo()
    }
}