package com.target.targetcasestudy.domain

import com.target.targetcasestudy.api.ServerResponse
import com.target.targetcasestudy.data.model.DealItem
import com.target.targetcasestudy.data.model.Result
import com.target.targetcasestudy.data.repository.DealDetailRepository
import com.target.targetcasestudy.data.repository.DealRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetDealDetailUseCase @Inject constructor(
    private val dealDetailRepository: DealDetailRepository
) {
    suspend fun fetchDealDataFromRepo(id: String): Result<DealItem>? {
        return dealDetailRepository.fetchDealDetail(id)
    }
}