package com.target.targetcasestudy.api

data class ServerResponse<T>(
    val success: Boolean,
    val products: List<T>
)