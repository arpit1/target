package com.target.targetcasestudy.api

import com.target.targetcasestudy.data.model.DealItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface DealApi {

    @GET("v1/deals")
    suspend fun getDealsList(): Response<ServerResponse<DealItem>>

    @GET("v1/deals/{id}")
    suspend fun getDealsDetail(
        @Path("id") id: String
    ): Response<DealItem>
}